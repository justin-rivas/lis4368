> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Justin Rivas

### Assignment 5 Requirements:

1. Server-side validation from A4
2. Compile servlet files
3. Pre and Post validation user form entry
4. MySQL customer table entry

#### README.md file should include the following items:

* Screenshot of A5 pre-validation user form entry
* Screenshot of A5 post-validation user form entry
* Screenshot of MySQL customer table entry
* Screenshot of Skillsets


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Assignment Screenshots:


*Screenshot of A5 pre-validation user form entry*:

![Pre-validation Screenshot](img/prevalidationform.png)

*Screenshot of A5 post-validation user form entry*:

![Post-validation Screenshot](img/postvalidationform.png)

*Screenshot of MySQL Customer table BEFORE entry*:

![Customer Table BEFORE Entry Screenshot](img/tableprevalidation.png)

*Screenshot of MySQL Customer table AFTER entry*:

![Customer Table AFTER Entry Screenshot](img/tablepostvalidation.png)

*Screenshot of Skillset 13 - File Write, Read, & Count Words*:

![Skillset 13 Screenshot](img/Q13screenshot.png)

*Screenshot of Skillset 14 - Vehicle Demo*:

![Skillset 14 Screenshot](img/Q14screenshot.png)

*Screenshot of Skillset 15 - Car Inherits Vehicle*:

![Skillset 15 Screenshot](img/Q15screenshot.png)

#### Document Links

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/justin-rivas/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/justin-rivas/myteamquotes/ "My Team Quotes Tutorial")
