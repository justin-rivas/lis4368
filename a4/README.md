> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Justin Rivas

### Assignment 4 Requirements:

1. Server-side validation
2. Compile servlet files
3. Pre and Post validation user form entry
4. Skillsets 10-12

#### README.md file should include the following items:

* Screenshot of A4 pre-validation user form entry
* Screenshot of A4 post-validation user form entry
* Screenshot of Skillsets


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Assignment Screenshots:


*Screenshot of A4 failed validation user form entry*:

![Failed validation Screenshot](img/failedvalidation.png)

*Screenshot of A4 successful user form entry*:

![Successful validation Screenshot](img/successfulvalidation.png)

*Screenshot of Skillset 10 - Simple Calculator*:

![Skillset 10 Screenshot](img/Q10screenshot.png)

*Screenshot of Skillset 11 - Array Copy*:

![Skillset 11 Screenshot](img/Q11screenshot.png)

*Screenshot of Skillset 12 - File, Write, Read, Count Words*:

![Skillset 12 Screenshot](img/Q12screenshot.png)

#### Document Links

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/justin-rivas/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/justin-rivas/myteamquotes/ "My Team Quotes Tutorial")
