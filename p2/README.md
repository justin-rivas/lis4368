> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Justin Rivas

### Project 2 Requirements:

1. Check cloned files
2. Compile Servlet Files
3. P2 Questions
4. Each P2 page screenshot


#### README.md file should include the following items:

* Screenshot of Valid data entry
* Screenshot of Thanks Page (thanks.jsp)
* Screenshot of Customers table (customer.jsp)
* Screenshot of entering modified data (modify.jsp)
* Screenshot of Customers table after modifying data
* Screenshot of the delete warning in Customers table
* Screenshot of each change in mySQL


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Project 2 Screenshots:

*Screenshot of Valid Data Entry - P2*:

![Valid Data Entry Screenshot](img/validdata.png)

*Screenshot of Thanks Page (thanks.jsp) - P2*:

![Thanks Page Screenshot](img/thankspage.png)

*Screenshot of Customers Table - P2*:

![Customers Table Screenshot](img/customers.png)

*Screenshot of Update/Modify Page (modify.jsp) - P2*:

![Modify Page Screenshot](img/updatepage.png)

*Screenshot of Customers table with Modified Data - P2*:

![Modified Customer Table Screenshot](img/customersmodified.png)

*Screenshot of Delete Warning in Customer Table - P2*:

![Delete Warning Screenshot](img/deletewarning.png)

*Screenshot of each change in the mySQL Tables through Terminal*:

![SQL Results Screenshot](img/sqlresults.png)

#### Document Links


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/justin-rivas/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/justin-rivas/myteamquotes/ "My Team Quotes Tutorial")
