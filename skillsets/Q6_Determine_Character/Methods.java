import java.util.Scanner;

public class Methods {
    public static void getRequirements() {
        System.out.println("Developer: Justin Rivas");
        System.out.println("Program determines whther user-entered value is vowel, consonant, special character, or integer.");
        System.out.println("References:");
        System.out.println("ASCII Background: https://en.wikipedia.org/wiki/ASCII");
        System.out.println("ASCII Character Table: https://www.ascii-code.com/");
        System.out.println("Lookup Tables: https://www.lookuptables.com/");
    }

    public static void determineChar() {
        char ch = ' ';
        char chTest = ' ';
            Scanner sc = new Scanner(System.in);
        System.out.print("Please enter a character: ");
        ch = sc.next().charAt(0);
        chTest = Character.toLowerCase(ch);

        if(chTest == 'a' || chTest == 'e' || chTest == 'i' || chTest == 'o' || chTest == 'u' || chTest == 'y') {
            System.out.println(ch + " is a vowel. ACII value: " + (int) ch);
        }
        else if(ch >= '0' && ch <= '9') {
            System.out.println(ch + " is an int. ACII value: " + (int) ch);
        }
        else if((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z')) {
            System.out.println(ch + " is a consonant. ACII value: " + (int) ch);
        }
        else {
            System.out.println(ch + " is a special character. ACII value: " + (int) ch);
        }
    sc.close();
    }
}