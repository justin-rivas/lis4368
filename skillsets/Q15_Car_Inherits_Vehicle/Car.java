class Car extends Vehicle
{
    private float  speed;

    public Car()
    {
        super();

        System.out.println("\nInside car default constructor.");

        speed = 100;
    }

    public Car(String m, String d, int y, float sp)
    {
        super(m, d, y);
        System.out.println("\nInside car constructor with parameters.");
        speed = sp;
    }

    public Float getSpeed()
    {
        return speed;
    }

    public void setSpeed (float s)
    {
        speed = s;
    }

    public void print()
    {
        super.print();
        System.out.println(", Speed: " + speed);
    }

}