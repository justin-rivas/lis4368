import java.util.Scanner;

public class Methods
{
    //create method without returning any value (without object)
    public static void getRequirements()
    {
        //Display operational messages
        System.out.println("Developer: Justin Rivas");
        System.out.println("Program determines total number of characers in the line text, \nas well as number of times specific character is used.");
        System.out.println("Program displays character\'s ASCII value.");

        System.out.println("\nReferences:\n"
                + "ASCII Background: https://urldefense.com/v3/__https://en.wikipedia.org/wiki/ASCII*5Cn__;JQ!!Epnw_ITfSMW4!7npyEmbtFcAHhE68R5KsvQAmkbP0Nhrt14Zb-ya0ckRzUOs-3ddEAXCdUqx7-Te6-A$ "
                + "ASCII Character Table: https://urldefense.com/v3/__https://ascii-code.com/*5Cn__;JQ!!Epnw_ITfSMW4!7npyEmbtFcAHhE68R5KsvQAmkbP0Nhrt14Zb-ya0ckRzUOs-3ddEAXCdUqzY4GIwpQ$ "
                + "Lookup Tables: https://urldefense.com/v3/__https://www.lookuptables.com/__;!!Epnw_ITfSMW4!7npyEmbtFcAHhE68R5KsvQAmkbP0Nhrt14Zb-ya0ckRzUOs-3ddEAXCdUqzbH73Gtg$ ");
        System.out.println(); //print blank line
    }

    public static void characterInfo()
    {
        //initialize variables
        String str = "";
        char ch = ' '; //unlike empty string *must* include character -- here. space character!
        int len = 0;
        int num = 0;
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Please enter line of text: ");
        str = sc.nextLine();
        len = str.length();

        System.out.print("Please enter character to check: ");
        //next() function returns next token
        //Token: smallest element of a program meaningful to compiler/interpreter
        //Generally, identifiers keywords, literals, operators, and punctuations
        //Note: white space and comments not tokens - thought, separate tokens
        //Example: "I like this" ("I" is 1st token, "like" is second token, and "this" is third token)

        //chain intrinsic (aka built-in) methods:
        //captures first character from user-entered token
        ch = sc.next().charAt(0);

        for(int i = 0; i < len; i++)
        {
            if(ch == str.charAt(i))
            {
                ++num;
            }
        }
        System.out.println("\nNumber of characters in line of text: " + len);
        System.out.println("The character " + ch + " appears " + num + " time(s) in line of text.");
        System.out.println("ASCII value: " + (int) ch); //cast char to int
        sc.close();
    }
}