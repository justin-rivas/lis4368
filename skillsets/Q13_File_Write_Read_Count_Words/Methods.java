import java.util.Scanner;
import java.text.NumberFormat;
import java.util.Locale;
import java.io.*;

import javax.lang.model.util.ElementScanner14;

public class Methods 
{

    public static void getRequirements()
    {
        System.out.println("Developer: Justin Rivas");
        System.out.println("Program Requirements: ");
        System.out.println("Program captures user input, writes to and reads from the same file,");
        System.out.println("and counts the number of words in the file.");
       
    }

    public static void largerNum()
    {
        int num1 = 0;
        int num2 = 0;

        //scanner is initialized
          Scanner scnr = new Scanner(System.in);

        //user is prompted for input and inputs are stored to num1 and num2
        System.out.println("Please enter first interger: ");
        num1 = scnr.nextInt();
        
        System.out.println("Please enter second interger: ");
        num2 = scnr.nextInt();                                       

        //if statement will be used to evaluate which number is larger

        if ( num1 > num2 )
        {
            System.out.println( num1 + " is larger than " + num2 );
        }
        else if ( num2 > num1)
        {
            System.out.println( num2 + " is larger than " + num1 );
        }
        else 
        {
            System.out.println("The numbers entered are equal");
        }
    }

    public static void evaluateNum()
    {
        //num is initialized and user is prompted for input
        int num = 0;
        System.out.println("Please enter an interger value:");

        //scanner is set up for user input and stored to num which is then printed back out
        
        Scanner scnr = new Scanner(System.in); 
        num = scnr.nextInt();
        
        System.out.println("User entered: " + num);

        //if num is divisible by two print "Even" else print "Odd"
        
        if ( num % 2 == 0 )
        {
            System.out.println(num + " is even.");
        }

        else
        {      
            System.out.println(num + " is odd.");
        }
        

        
    }

    public static void arraysAndLoops()
    {
        String[] array = { "dog", "cat", "bird", "fish", "insect" };

        //for loop
        System.out.println("");
        System.out.println("for loop:");
        System.out.println("");
        for ( int i = 0; i < array.length; i++ )
        {
            System.out.println( array[i] );
        }

        //enhanced for loop
        System.out.println("");
        System.out.println("Enhanced for loop:");
        System.out.println("");
        for ( String x : array )
        {
            System.out.println( x );
        }

        //while loop
        System.out.println("");
        System.out.println("while loop:");
        System.out.println("");

        int c = 0;
        while ( c < array.length )
        {
            System.out.println( array[c] );
            c++;
        }

        //do while loop
        System.out.println("");
        System.out.println("do while loop:");
        System.out.println("");

        int v = 0;
        do
        {
            System.out.println( array[v] );
            v++;
            
        } while ( v < array.length );
    }

    public static void characterInfo()
    {
        //initialize variable objects
        String str = "";
        String temp = "";
        char search = 'a';
        int count = 0;
        int ascii = 0;

        //initialize scanner
        Scanner scnr = new Scanner(System.in);

        //Prompt user for string
        System.out.println("Please enter line of text: ");
        str = scnr.nextLine();

        System.out.println("Please enter character that you would like to search for: ");
        temp = scnr.next();
        search = temp.charAt(0);

        //count number of search occurences
        for( int i = 0; i<str.length(); i++ )
        {
            if(str.charAt(i) == search)
            count++;
        }

        //get ascii value
        ascii = (int) search;

        //print info
        System.out.println( "Total number of characters in text: " + str.length() );
        System.out.println( "The character " + search + " appears " + count + " times." );
        System.out.println( "The ASCII value of " + search + " is equal to " + ascii );


    }

    public static void determineCharacter()
    {
        //initialize variables
        char cha = 'a';
        char test = 'a';
        Scanner scnr = new Scanner(System.in);

        //prompt user 
        System.out.print("Please enter a character: ");
        cha = scnr.next().charAt(0);
        test = Character.toLowerCase(cha);

        //test for vowels
        if ( test == 'a' || test == 'e' || test == 'i' || test == 'o' || test == 'u' || test == 'y' )
        {
            System.out.println(cha + " is a vowel. ASCII value: " + (int) cha);
        }
        //test for consonant
        else if ( test >= 'a' && test <= 'z' )
        {
            System.out.println(cha + " is a consonant. ASCII value: " + (int) cha);
        }
        //test for int
        else if ( cha >= '0' && cha <= '9' )
        {
            System.out.println(cha + " is an interger. ASCII value: " + (int) cha);
        }
        else 
        {
            System.out.println(cha + " is a special character. ASCII value: " + (int) cha);
        }

    }

    public static void countCharacters()
    {
        //variable time
        int letter = 0;
        int space = 0;
        int num = 0;
        int upper = 0;
        int lower = 0;
        int special = 0;
        String string = "";

        Scanner scnr = new Scanner(System.in);

        //get string from user
        System.out.print("Please enter string: ");
        string = scnr.nextLine();

        //populate array with string
        char[] cha = string.toCharArray();

        //use loop to parse through array and increment int variables accordingly
        for ( int i = 0; i < cha.length; i++ )
        {
            if ( Character.isLetter(cha[i]) )
            {
                if ( Character.isUpperCase(cha[i]) )
                {
                    upper++;
                }
                if ( Character.isLowerCase(cha[i]) )
                {
                    lower++;
                }
                
                letter++;
            }
            else if ( Character.isDigit(cha[i]) )
            {
                num++;
            }
            else if ( Character.isSpaceChar(cha[i]) )
            {
                space++;
            }
            else
            {
                special++;
            }
        }

        System.out.println("\nYour string: (" + string + ") has the following number and type of characters.");
        System.out.println("Total number of characters: " + cha.length);
        System.out.println("Letter(s): " + letter);
        System.out.println("Upper case letter(s): " + upper);
        System.out.println("Lower case letter(s):");
        System.out.println("Number(s): " + num);
        System.out.println("Space(s): " + space);
        System.out.println("Special Character(s): " + special);

        scnr.close();

    }

    public static void displayASCII()
    {
        //variables
        int num = 0; 
        boolean isValidNum = false;
        Scanner scnr = new Scanner(System.in);

        System.out.println("Printing characters A-Z with ASCII values:");

        //use for loop to print the stuff
        for ( char character = 'A'; character <= 'Z'; character++)
        {
            System.out.printf("Character %c has ASCII value %d\n", character, ((int)character));
        }
        for ( num = 48; num <= 122; num++ )
        {
            System.out.printf("ASCII value %d has the character value %c\n", num, ((char)num));
        }

        //user input part
        System.out.println("\nAllowing user ASCII value input: ");

        while ( isValidNum == false)
        {
            //make sure they put in a nnumber
            System.out.print("Please enter ASCII value from range 32-127: ");
            if (scnr.hasNextInt())
            {
                num = scnr.nextInt();
                isValidNum = true;
            }
            else
            {
                System.out.println("Invalid interger, user must input a number for ASCII value.\n");
            }
            scnr.nextLine();
            
            //make sure the number is within the range
            if ( isValidNum == true && num < 32 || num > 127 )
            {
                System.out.println("ASCII value must be between 32 and 127 including those numbers.\n");
                isValidNum = false;
            }
            if ( isValidNum == true )
            {
                System.out.println();
                System.out.printf("ASCII value %d has character value %c\n", num, ((char)num));
            }
        }
        scnr.close();
    }

    public static float getFloat(String whichNum)
    {
        //initialize objects
        Scanner scnr = new Scanner(System.in);
        float num = 0f;

        //prompt user
        System.out.print("Please enter " + whichNum + ": ");

        //validation
        while (!scnr.hasNextFloat())
        {
            System.out.println("Not a valid float!");
            scnr.next();
            System.out.print("Please enter a valid float: ");
        }
        System.out.println("");
        num = scnr.nextFloat();

        return num;
    }

    public static float getGrade(String whichNum)
    {
        //initialize objects
        Scanner scnr = new Scanner(System.in);
        float num = 0f;
        boolean isValidNum = false;

        //prompt user
       // System.out.print("Please enter " + whichNum + ": ");

        //validation
        while ( isValidNum == false)
        {
            //make sure they put in a nnumber
            System.out.print("Please enter " + whichNum + ": ");
            if (scnr.hasNextFloat())
            {
                num = scnr.nextFloat();
                isValidNum = true;
            }
            else
            {
                System.out.println("Invalid grade, must be a number.\n");
            }
            scnr.nextLine();
            
            //make sure the number is within the range
            if ( isValidNum == true && num < 0 || num > 100 )
            {
                System.out.println("Grade must be between 0 and 100 inclusive.\n");
                isValidNum = false;
            }
            
        }

        return num;
    }

    public static void gradeCalculator()
    {
        //variables
        float gradeNum = 0f;
        String finalGrade = "";

        //get float for grade
        gradeNum = getGrade("a grade between 0 and 100 inclusive");

        if ( gradeNum >= 93 )
        {
            finalGrade = "A";
        }
        else if ( gradeNum >= 90 && gradeNum < 93 )
        {
            finalGrade = "A-";
        }
        else if ( gradeNum >= 87 && gradeNum < 90)
        {
            finalGrade = "B+";
        }
        else if ( gradeNum >= 83 && gradeNum < 87 )
        {
            finalGrade = "B";
        }
        else if ( gradeNum >= 80 && gradeNum < 83 )
        {
            finalGrade = "B-";
        }
        else if ( gradeNum >= 77 && gradeNum < 80 )
        {
            finalGrade = "C+";
        }
        else if ( gradeNum >= 73 && gradeNum < 77 )
        {
            finalGrade = "C";
        }
        else if ( gradeNum >= 70 && gradeNum < 73 )
        {
            finalGrade = "C-";
        }
        else if ( gradeNum >= 67 && gradeNum < 70 )
        {
            finalGrade = "D+";
        }
        else if ( gradeNum >= 63 && gradeNum < 67 )
        {
            finalGrade = "D";
        }
        else if ( gradeNum >= 60 && gradeNum < 63 )
        {
            finalGrade = "D-";
        }
        else
        {
            finalGrade = "F";
        }

        //program outputs
        System.out.println("Score entered: " + gradeNum);
        System.out.println("Final grade: " + finalGrade);
    }

    public static void simpleCalculator()
    {
    //declare variables and create Scanner object
     double num1 = 0.0; 
     double num2 = 0.0;
     char operation = ' ';
     
     Scanner sc = new Scanner (System.in);
     
    System.out.print("Enter mathematical operation (a =addition, s=subtraction, m=multiplicaation, d=division, e=exponentiation): ");    

    operation = sc.next().toLowerCase().charAt(0);

    while (operation !='a' && operation !='s' && operation !='m' && operation !='d' && operation !='e')
    {
      System.out.print("\nInvalid operation. Please enter valid operation: "); 
      operation = sc.next().toLowerCase().charAt(0); 
    
    }
    
     System.out.print("Please enter first number: "); 
     while (!sc.hasNextDouble())
     {
       System.out.println("Not a valid number!\n");
       sc.next();
       System.out.print("Please try again. Please enter first number: ");   
     }
     num1 = sc.nextDouble();
    
    System.out.print("Please enter second number: "); 
     while (!sc.hasNextDouble())
     {
       System.out.println("Not a valid number!\n");
       sc.next();
    System.out.print("Please try again. Enter second number: "); 
     }
    num2 = sc.nextDouble();
    
    
    if (operation == 'a')
    {
        Addition(num1, num2);
    }

    else if (operation == 's')
    {
        Subtraction(num1, num2);
    }

    else if (operation == 'm')
    {
        Multiplication(num1, num2);
    }

    else if (operation == 'd')
    {
        if (num2 == 0)
        {
        System.out.println("Cannot divide by zero...");
    }
    else
        {
        Division(num1, num2);
        }
    }

     else if (operation == 'e')
    {
        Exponentiation(num1, num2);
    }

    System.out.println();  //blank line 
    
    sc.close();
    }
    
    
    public static void Addition(double n1, double n2)
    {
    System.out.print(n1 + " + " + n2 + " = "  );
    System.out.printf("%.2f", (n1 + n2));
    }
       
    public static void Subtraction(double n1, double n2)  
    {
    System.out.print(n1 + " - " + n2 + " = "  );
    System.out.printf("%.2f", (n1 - n2));
    }
        
    public static void Multiplication(double n1, double n2)
    {
    System.out.print(n1 + " * " + n2 + " = "  );
    System.out.printf("%.2f", (n1 * n2));
    }
    
    public static void Division(double n1, double n2)
    {
    System.out.print(n1 + " / " + n2 + " = "  );
    System.out.printf("%.2f", (n1 / n2));
    }
    
    public static void Exponentiation(double n1, double n2)
    {
    System.out.print(n1 + " ^ " + n2 + " = "  );
    System.out.printf("%.2f", (Math.pow(n1, n2)));
    }

    public static void compoundInterestCalculator()
    {
        //initialize variables
         Scanner sc = new Scanner (System.in);
         double principal = 0.0;
         double rate = 0.0;
         int time = 0;
    
        System.out.print("Enter current principal: $");    
         while(!sc.hasNextDouble())
         {
            System.out.println("Not a valid number!\n");
            sc.next();
            System.out.print("Please try again. Enter principal:  $");   
         }
         principal = sc.nextDouble();
        
        System.out.print("Enter Interest Rate (per year): ");    
         while(!sc.hasNextDouble())
         {
            System.out.println("Not a valid number!\n");
            sc.next();
            System.out.print("Please try again. Enter Interest Rate: ");   
         }
         rate = sc.nextDouble();
         rate = rate /100;
    
        System.out.print("Total time (in years): ");    
         while(!sc.hasNextInt())
         {
            System.out.println("Not a valid integer!\n");
            sc.next();
            System.out.print("Please try again. Enter years: ");   
         }
         time = sc.nextInt();
    
         sc.close();
        
        
        calculateInterest(principal, rate, time);
    }

    public static void calculateInterest(double p, double r, int t)
    {
    //initialize variables
    int n = 12;
    double i = 0.0;
    double a = 0.0;

    a = p * Math.pow(1 + (r/n), n * t);
    i = a-p;

    r = r * 100;
     
    NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
    System.out.println("\nInitial Principal: " + nf.format(p));

    System.out.printf("Annual Interest Rate: %.1f%%%n", r);
    System.out.println("Total monthly compounded interest after: " + t + " years " + nf.format(i));
    System.out.println("Total amount: " + nf.format(a));
    }

    public static String[] createArray()
    {
    Scanner sc = new Scanner (System.in);
    int arraySize = 0;



    System.out.print("How many favorite programming languages do you have (min 1)?    ");    
    while(!sc.hasNextInt())
     {
       System.out.println("Invalid input!\n");
       sc.next();    
       System.out.print("Please try again. Enter valid input (minimum 1):    "); 
     }
    arraySize = sc.nextInt();

    while (arraySize < 1)
     {

        System.out.print("\nList must be greater than 0. Please enter number of favorite programming languages:  ");   
        while(!sc.hasNextInt())
        {
        System.out.println("\nMust enter valid data:     ");
        sc.next();    
        System.out.print("Please try again. Enter valid input (minimum 1):    "); 
        }
     arraySize = sc.nextInt();
     }
     
     
     
     String yourArray[] = new String[arraySize];
     return yourArray;
    }
     

    public static void copyArray(String [] str1)
    {
    Scanner sc = new Scanner (System.in);

    System.out.println("Please enter your " + str1.length + "favorite progrmming languages:  ");
    for (int i = 0; i < str1.length; i++)
    {
     str1[i] = sc.nextLine();
     }


    String str2[] = new String[str1.length];



    int myCounter = 0;
    for(String myIterator: str1)
    {
        str2[myCounter++] = myIterator;
    }


    System.out.println();  // print blank line


    System.out.println("Printing str1 array:   ");  
    for (String myIterator: str1)
    {
      System.out.println(myIterator);  
    }

    System.out.println("\nPrinting str2 array:   ");  
    for (String myIterator: str2)
    {
      System.out.println(myIterator);  
    }

     
     
      System.out.println();  // print blank line

     sc.close();
    }
    
    public static void getWriteRead()
    {
        //initalize variables
        String myFile = "filecountwords.txt";

        try{

        //create file objec
        File file = new File(myFile);

        //create printwriter
        PrintWriter writer = new PrintWriter(file);

        //create scanner
        Scanner input = new Scanner(System.in);

        //string for usr input
        String str = "";

        System.out.print("Please enter line of text: ");
        str = input.nextLine();

        //write str to file
        writer.write(str);

        System.out.println("Saved to file '" + myFile + "'" );

        //close scanner and input file
        input.close();
        writer.close();
        Scanner read  = new Scanner(new FileInputStream(file));
        int count = 0;
        while(read.hasNext())
        {
            read.next();
            count ++;
        }
        System.out.println("Number of words: " + count);

        read.close();
        }
        catch(IOException ex)
        {
            System.out.println("Error writing to or reading from file '" + myFile + "'");
        }
    }

}