> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Justin Rivas

### Assignment 1 Requirements:

1. Git and Bitbucket Setups
2. Development Installations
3. A1 Questions
4. Bitbucket repo links

#### README.md file should include the following items:

* Screenshot of A1 index.jsp
* Screenshot of JDK
* Screenshot of Tomcat


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch 
7. git tag - Create, list, and delete or verify a tag object signed with GPG

#### Assignment Screenshots:

*Screenshot of a1/index.jsp http://localhost*:9999

![A1/index.jsp Installation Screenshot](a1screenshot.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](jdkscreenshot.png)

*Screenshot of Tomcat - Apache Tomcat*:

![Tomcat Installation Screenshot](tomcat.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/justin-rivas/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/justin-rivas/myteamquotes/ "My Team Quotes Tutorial")
