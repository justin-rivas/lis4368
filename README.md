> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4368 - Mobile Application Development

## Justin Rivas

### LIS 4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Visual Studio Code
    - Download Tomcat
    - Provide Installation Screenshots
    - Complete Bitbucket Tutorial
    - Edit index.jsp

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Configure mySQL Workbench through AMPPS
    - Write "Hello World" Java Servlet
    - Create eBookshop database through mySQL Workbench
    - Edit index.jsp
    - Provide screenshots of each servlet and server hosted using Tomcat

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create ERD in mySQL workbench
    - ERD screenshots
    - A3 Questions
    - Servlet compilation and invocation
    - A3 index.jsp screenshot
    - Skillset screenshots

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Write and add code for jQuery Validations
    - Screenshot of a failed validation
    - Screenshot of a successful validation
    - P1 Questions
    - P1 index.jsp screenshot
    - Skillset screenshots

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Server-side validation
    - Compile servlet files
    - Pre and Post validation user form entry
    - Skillsets 10-12


6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Server-side validation from A4
    - Compile servlet files
    - Pre and Post validation user form entry
    - MySQL customer table entry
    - Skillset screenshots

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Review Cloned Files
    - Update & Compile servlet files 
    - Revise modify.jsp, thanks.jsp, & customers.jsp as needed
    - Select, Insert, Update, & Delete changes in the mySQL table
    - P2 Assignment Questions
    
