> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Justin Rivas

### Assignment 3 Requirements:

1. Create ERD in mySQL workbench
2. ERD screenshots
3. A3 Questions
4. Servlet compilation and invocation
5. A3 index.jsp screenshot
6. Skillset screenshots

#### README.md file should include the following items:

* Screenshot of A3 index.jsp
* Screenshot of ERD
* Screenshot of each Table with Data Inserts
* Screenshot of Skillsets


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Assignment Screenshots:

*Screenshot of a3/index.jsp http://localhost:9999/lis4368/a3/index.jsp

![A3/index.jsp Installation Screenshot](img/a3screenshot.png)

*Screenshot of ERD Diagram - mySQL*:

![ERD Screenshot](img/ERDscreenshot.png)

*Screenshot of First Table - Petstore*:

![Petstore Table Screenshot](img/table1screenshot.png)

*Screenshot of Second Table - Customer*:

![Customer Table Screenshot](img/table2screenshot.png)

*Screenshot of Third Table - Pet*:

![Pet Table Screenshot](img/table3screenshot.png)

*Screenshot of Skillset 4 - System Properties*:

![Skillset 4 Screenshot](img/Q4screenshot.png)

*Screenshot of Skillset 5 - Looping Structures*:

![Skillset 5 Screenshot](img/Q5screenshot.png)

*Screenshot of Skillset 6 - Number Swap*:

![Skillset 6 Screenshot](img/Q6screenshot.png)

#### Document Links

*Link to a3.mwb*:
[a3.mwb](docs/A3.mwb "Link to Assignment 3 MySQL Workbench database document")
<!--
If you dont want alt text, do this:
[a3.mwb](docs/a3.mwb)
-->

*Link to a3.sql*:
[a3.sql](docs/a3.sql "Link to Assignment 3 MySQL document")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/justin-rivas/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/justin-rivas/myteamquotes/ "My Team Quotes Tutorial")
