> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Justin Rivas

### Project 1 Requirements:

1. Set-up cloned files for P1
2. Add jQuery validation and regular expressions
3. P1 Questions
4. P1 index.jsp screenshot
5. Skillsets 7-9 screenshots

#### README.md file should include the following items:

* Screenshot of P1 index.jsp
* Screenshot of Failed and Successful jQuery Validations
* Screenshot of Main page for P1
* Screenshot of Skillsets


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Assignment Screenshots:

*Screenshot of p1/index.jsp http://localhost:9999/lis4368/p1/index.jsp

![P1/index.jsp Main Page Screenshot](img/mainpage.png)

*Screenshot of Failed Validation - P1*:

![Failed Validation Screenshot](img/validationfail.png)

*Screenshot of Successful Validation - P1*:

![Successful Validation Screenshot](img/validationcheck.png)

*Screenshot of Skillset 7 - Count Characters*:

![Skillset 7 Screenshot](img/Q7screenshot.png)

*FIRST Screenshot of Skillset 8 - ASCII*:

![First Skillset 8 Screenshot](img/Q8screenshot1.png)

*SECOND Screenshot of Skillset 8 - ASCII*:

![Second Skillset 8 Screenshot](img/Q8screenshot2.png)

*Screenshot of Skillset 9 - Grade Calculator*:

![Skillset 9 Screenshot](img/Q9screenshot.png)

#### Document Links

*Link to P1/index.jsp*:
[p1](index.jsp "Link to P1 index.jsp")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/justin-rivas/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/justin-rivas/myteamquotes/ "My Team Quotes Tutorial")
