> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Justin Rivas

### Assignment 2 Requirements:

1. Install mySQL Workbench 
2. A2 Questions
3. Servlet compilation and invocation
4. A2 index.jsp editing

#### README.md file should include the following items:

* Screenshot of A2 index.jsp
* Screenshot of Servlets
* Screenshot of Hello World
* Screenshot of Skillsets


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Assignment Screenshots:

*Screenshot of a2/index.jsp http://http://localhost:9999/lis4368/a2/index.jsp

![A2/index.jsp Installation Screenshot](img/a2screenshot.png)

*Screenshot of Querybooks*

![Querybooks Screenshot](img/querybook.png)

*Screenshot of Querybooks Results*

![Querybooks Screenshot](img/queryresults.png)

*Screenshot of https://localhost:9999/hello/*:

![localhost:9999/hello/ Installation Screenshot](img/hello.png)

*Screenshot of https://localhost:9999/hello/sayhello/*:

![localhost:9999/hello/sayhello Installation Screenshot](img/helloworld.png)

*Screenshot of https://localhost:9999/hello/HelloHome/*:

![localhost:9999/hello/HelloHome Installation Screenshot](img/hellohome.png)

*Screenshot of Skillset 1 - System Properties*:

![Skillset 1 Screenshot](img/Q1.png)

*Screenshot of Skillset 2 - Looping Structures*:

![Skillset 2 Screenshot](img/Q2.png)

*Screenshot of Skillset 3 - Number Swap*:

![Skillset 3 Screenshot](img/Q3.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/justin-rivas/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/justin-rivas/myteamquotes/ "My Team Quotes Tutorial")
